### ReadMe

This website is developed with webpack, to see it in developmenet mode, clone this repository and install node dependencies (npm install).

Run the command npm run dev to run it locally on localhost:4444

Responsive layouts are based on devices; Iphone 8, Ipad Classic and Desktop.