import { onLoadBestSellers } from "./scripts/bestSeller";
import { onLoadCategories } from "./scripts/category";
import { onLoadCover } from "./scripts/cover";
import { onLoadNewProductList } from "./scripts/products";
import { onLoadBanner } from "./scripts/imageBanner";
import { toggleMenu } from "./scripts/events";

import "./styles/styles.scss";

function onSubmit() {
  console.log("Submited Newsletter");
}

var addListeners = () => {
  document.getElementById("menu-button").addEventListener("click", toggleMenu);
  document.getElementById("newsletter").addEventListener("submit", onSubmit);
};

window.onload = () => {
  onLoadCover();
  onLoadNewProductList();
  onLoadCategories();
  onLoadBestSellers();
  onLoadBanner();
  addListeners();
};
