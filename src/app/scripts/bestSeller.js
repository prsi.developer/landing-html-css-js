var bestSellers = [
  {
    name: "Jacket",
    brand: "North Face",
    price: "99,99€",
    imageSrc:
      "https://ecoalf.com/18590-home_default/abrigo-hombre-canada.jpg",
  },
  {
    name: "Pants",
    brand: "North Face",
    price: "99,99€",
    imageSrc:
      "https://ecoalf.com/18590-home_default/abrigo-hombre-canada.jpg",
  },
  {
    name: "T-Shirt",
    brand: "North Face",
    price: "99,99€",
    imageSrc:
      "http://cdn.shopify.com/s/files/1/1018/6883/products/CATOWN-VERDETE_2_grande.jpg?v=1611851062",
  },
  {
    name: "Beanie",
    brand: "North Face",
    price: "99,99€",
    imageSrc:
      "https://i.pinimg.com/originals/dc/41/82/dc4182a06e6a4a4fd0815a775dbd05ab.jpg",
  },
  {
    name: "Boots",
    brand: "North Face",
    price: "99,99€",
    imageSrc:
      "https://media.theeverygirl.com/wp-content/uploads/2017/10/the-everygirl-how-to-style-overtheknee-boots-1.jpg",
  },
  {
    name: "Shirt",
    brand: "North Face",
    price: "99,99€",
    imageSrc:
      "http://cdn.shopify.com/s/files/1/1018/6883/products/CATOWN-VERDETE_2_grande.jpg?v=1611851062",
  },
  {
    name: "Dress",
    brand: "North Face",
    price: "99,99€",
    imageSrc:
      "https://media.theeverygirl.com/wp-content/uploads/2017/10/the-everygirl-how-to-style-overtheknee-boots-1.jpg",
  },
  {
    name: "Rain Jacket",
    brand: "North Face",
    price: "99,99€",
    imageSrc:
      "https://ecoalf.com/18590-home_default/abrigo-hombre-canada.jpg",
  },
];

function onLoadBestSellers() {
  var renderBestSellers = document.getElementById("bestSellers__list");
  bestSellers.forEach((product) => {
    var li = document.createElement("li");
    li.setAttribute("class", "bestSellers__item");
    li.innerHTML = `<img classname="bestSellers__item--img" src="${product.imageSrc}" alt="${product.name}" /><h5 classname="bestSellers__item--title">${product.name}</h5><p classname="bestSellers__item--brand">${product.brand}</p><p classname="bestSellers__item--price">${product.price}</p><a href="#">Shop now</a>`;
    renderBestSellers.appendChild(li);
  });
}

export { onLoadBestSellers };
