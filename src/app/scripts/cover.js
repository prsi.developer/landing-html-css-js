var cover = [
  {
    imageSrc:
      "https://rqncr2pq51u34xp5y2t8cvy1-wpengine.netdna-ssl.com/wp-content/uploads/2016/02/Match-Snowboard-to-Your-Favorite-Conditions.jpg",
    name: "Brand Name",
    logo: "https://www.regolodesign.com/assets/img/brands/logo_Koibird.png",
    quote: "We have amazing clothing",
  },
];

const onLoadCover = () => {
  var renderCover = document.getElementById("cover__container");
  cover.map((cover) => {
    var div = document.createElement("div");
    div.setAttribute("class", "cover__container");
    div.innerHTML = `<img class="cover__image" src="${cover.imageSrc}" alt="${cover.name}" /><div class="cover__logo"><img class="cover__logo--img" src="${cover.logo}" alt="${cover.logo}" /><p class="cover__text">${cover.quote}</p></div>`;
    renderCover.appendChild(div)
  });
};

export {
    onLoadCover
}