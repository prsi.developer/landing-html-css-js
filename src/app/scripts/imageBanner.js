var brandName = [
  {
    title: "#koiko",
  },
];
var pictures = [
    {
        name: "OuterWear",
        imageSrc: "https://ecoalf.com/18590-home_default/abrigo-hombre-canada.jpg",
      },
      {
        name: "Summer",
        imageSrc: "http://cdn.shopify.com/s/files/1/1018/6883/products/CATOWN-VERDETE_2_grande.jpg?v=1611851062",
      },
      {
        name: "Woman",
        imageSrc: "https://media.theeverygirl.com/wp-content/uploads/2017/10/the-everygirl-how-to-style-overtheknee-boots-1.jpg",
      },
      {
        name: "Accesories",
        imageSrc: "https://i.pinimg.com/originals/dc/41/82/dc4182a06e6a4a4fd0815a775dbd05ab.jpg",
      },
      {
        name: "Googles",
        imageSrc: "https://ecoalf.com/18590-home_default/abrigo-hombre-canada.jpg",
      },
      {
        name: "Jackets",
        imageSrc: "https://ecoalf.com/18590-home_default/abrigo-hombre-canada.jpg",
      },
      {
        name: "Accesories",
        imageSrc: "https://i.pinimg.com/originals/dc/41/82/dc4182a06e6a4a4fd0815a775dbd05ab.jpg",
      },
      {
        name: "Accesories",
        imageSrc: "https://i.pinimg.com/originals/dc/41/82/dc4182a06e6a4a4fd0815a775dbd05ab.jpg",
      },
];

function onLoadBannerImages() {
  var renderPictures = document.getElementById("brandImages__list");
  pictures.forEach((picture) => {
    var a = document.createElement("a");
    a.setAttribute("href", "#");
    a.innerHTML = `<img classname="brandImages__img" src="${picture.imageSrc}" alt="${picture.name}" />`;
    renderPictures.appendChild(a);
  });
}
function onLoadBannerTitle() {
  var renderBrandName = document.getElementById("brandImages__container");
  brandName.map((brand) => {
    var div = document.createElement("div");
    div.setAttribute("class", "brandImages__title");
    div.innerHTML = `<h3>${brand.title}</h3>`;
    renderBrandName.appendChild(div);
  });
}
function onLoadBanner() {
  onLoadBannerTitle();
  onLoadBannerImages();
}

export { onLoadBanner };
