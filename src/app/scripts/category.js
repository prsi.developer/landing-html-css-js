var categories = [
  {
    name: "OuterWear",
    imageSrc: "https://ecoalf.com/18590-home_default/abrigo-hombre-canada.jpg",
  },
  {
    name: "Summer",
    imageSrc: "http://cdn.shopify.com/s/files/1/1018/6883/products/CATOWN-VERDETE_2_grande.jpg?v=1611851062",
  },
  {
    name: "Woman",
    imageSrc: "https://media.theeverygirl.com/wp-content/uploads/2017/10/the-everygirl-how-to-style-overtheknee-boots-1.jpg",
  },
  {
    name: "Accesories",
    imageSrc: "https://i.pinimg.com/originals/dc/41/82/dc4182a06e6a4a4fd0815a775dbd05ab.jpg",
  },
  {
    name: "Googles",
    imageSrc: "https://ecoalf.com/18590-home_default/abrigo-hombre-canada.jpg",
  },
  {
    name: "Jackets",
    imageSrc: "https://ecoalf.com/18590-home_default/abrigo-hombre-canada.jpg",
  },
];

function onLoadCategories() {
  var renderCategories = document.getElementById("categories__list");
  categories.forEach((category) => {
    var li = document.createElement("li");
    li.setAttribute("class", "category__item");
    li.innerHTML = `<img classname="category__item--img" src="${category.imageSrc}" alt="${category.name}" /><a href="#" classname="category__item--title">${category.name}</a>`;
    renderCategories.appendChild(li);
  });
}

export { onLoadCategories };
